# frozen_string_literal: true

module QueryMagic
  module Filters
    def self.simple(column:, arel_selector:)
      MagicMethod.new <<-RUBY
@relation = @relation.where(@relation.arel_table[:#{column}].public_send(:#{arel_selector}, val))
RUBY
    end

    def self.static(column:, arel_selector:, val:)
      MagicMethod.new(<<-RUBY, static_val: val)
@relation = @relation.where(@relation.arel_table[:#{column}].public_send(:#{arel_selector}, val))
RUBY
    end

    def self.join_filter(join_name:, join_class:, column:, arel_selector:)
      MagicMethod.new <<-RUBY
@relation = @relation
              .joins(:#{join_name})
              .merge(#{join_class}.where(#{join_class}.arel_table[:#{column}].public_send(:#{arel_selector}, val)))
RUBY
    end

    def self.join_filter_static(join_name:, join_class:, column:, arel_selector:, val:)
      MagicMethod.new(<<-RUBY, static_val: val)
@relation = @relation
              .joins(:#{join_name})
              .merge(#{join_class}.where(#{join_class}.arel_table[:#{column}].public_send(:#{arel_selector}, val)))
RUBY
    end
  end
end
