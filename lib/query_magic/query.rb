# frozen_string_literal: true

module QueryMagic
  class Query
    attr_accessor :relation # ActiveRecord_Relation
    extend Forwardable
    def_delegators :@relation, :to_a, :count

    @default_relation = nil

    def self.default_relation(&b)
      @default_relation = b
    end

    def self.define_filter(method_name, **kwargs)
      Filters.simple(**kwargs).apply(self, method_name)
    end

    def self.define_static_filter(method_name, **kwargs)
      Filters.static(**kwargs).apply(self, method_name)
    end

    def self.define_join_filter(method_name, **kwargs)
      Filters.join_filter(**kwargs).apply(self, method_name)
    end

    def self.define_static_join_filter(method_name, **kwargs)
      Filters.join_filter_static(**kwargs).apply(self, method_name)
    end

    def initialize
      if default_relation.respond_to?(:call)
        @relation = default_relation.call
      else
        raise QueryMagic::Error, "no default_relation set"
      end
    end

    def where(...)
      @relation = @relation.where(...)
      self
    end

    def order(...)
      @relation = @relation.order(...)
      self
    end

    def select(...)
      @relation = @relation.select(...)
      self
    end

    private

    def default_relation
      self.class.instance_variable_get(:@default_relation)
    end
  end
end
