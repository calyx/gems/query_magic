# frozen_string_literal: true

module QueryMagic
  class MagicMethod
    class NOVAL; end;

    def initialize(script, static_val: NOVAL)
      @script = script

      if static_val == NOVAL
        @before_script = ""
        @argument = "(val)"
      else
        @before_script = "val = #{stringify(static_val)}"
        @argument = ""
      end
    end

    def apply(target, method_name)
      target.module_eval <<-RUBY
def #{method_name}#{@argument}
  #{@before_script}
  #{@script}
  self
end
RUBY
    end

    private

    def stringify(val)
      case val
      when NilClass
        "nil"
      when Integer, Float, TrueClass, FalseClass
        val.to_s
      when String, Date
        "\"#{val}\""
      when Symbol
        ":#{val}"
      else
        raise TypeError, "Invalid type #{val.class}"
      end
    end
  end
end
