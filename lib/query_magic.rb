# frozen_string_literal: true

require 'forwardable'

require_relative "query_magic/version"
require_relative "query_magic/magic_method"
require_relative "query_magic/filters"
require_relative "query_magic/query"

module QueryMagic
end
